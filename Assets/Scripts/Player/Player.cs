﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

	//Public Variables
	public int Health, curHealth;
	public float forwardSpeed;
	public float sideSpeed;
	public Transform bulletSpawn, weaponSpot, objectSpot;
	public GameObject bulletPrefab, deathPrefab;
	public AudioClip footsteps;
	public GameObject currentObject;
	[HideInInspector]
	public bool inputEnabled, shootLaser;
	//Weapon Variables
	public Weapon currentWeapon;
	public Weapon weaponInRange;
	public bool reload;
	//Private variables
	private Rigidbody rb;
	private bool openScreen, charge;
	private AudioSource playerAudio;
	//Local variables
	GameObject currentRock, currentCharge;
	float scale = 1f;
	float maxScale = 25f;
	float scaleSpeed = 8f;
	float chargeDamage = 12f;
	float fireRate = 0.5f;
	float nextFire = 0.0f;

	void Start ()
	{
		curHealth = Health;
		inputEnabled = true;
		rb = GetComponent<Rigidbody> ();
		playerAudio = GetComponent<AudioSource> ();
	}

	void Update ()
	{
		HandleAim ();
		if (openScreen)
			return;
		HandleShooting ();
		HandleLaser ();
		HandleAudio ();
		if (charge) {
			HandleCharge ();
			if (currentCharge.transform.localScale.x >= 25f) {
				currentCharge.GetComponent<Renderer> ().material.color = Color.cyan;
			} else {
				Color temp = Color.Lerp (Color.yellow, Color.red, Mathf.PingPong (Time.time * 4, 1));
				currentCharge.GetComponent<Renderer> ().material.color = temp;
			}
		}
	}

	void FixedUpdate ()
	{
		HandleInput ();
		if (openScreen) {
			OpenScreen (GameManager.instance.openScreenAnimator);
		} else {
			GameManager.instance.openScreenAnimator.SetBool ("OpenScreen", false);
		}
	}

	public void UpdateHealthbar ()
	{
		StartCoroutine (SwitchMat ());
		GameManager.instance.playerHealthSlider.value = curHealth;

		if (curHealth <= 0) {
			DropWeapon ();
			GameManager.instance.Invoke("RespawnPlayer",1.5f);
			Instantiate (deathPrefab, transform.position, Quaternion.identity);
			Destroy (gameObject);
		}

	}

	IEnumerator SwitchMat ()
	{
		Color temp = GetComponent<Renderer> ().material.color;
		GetComponentsInChildren<Renderer> () [1].material.color = Color.red;
		yield return new WaitForSeconds (.25f);
		GetComponentsInChildren<Renderer> () [1].material.color = temp;

	}

	void OpenScreen (Animator ani)
	{
		ani.SetBool ("OpenScreen", true);
	}

	void HandleInput ()
	{
		if (!inputEnabled)
			return;
		// Movement Controls
		if (Input.GetKey (KeyCode.W)) {
			rb.AddForce (transform.forward * forwardSpeed);
		}

		if (Input.GetKey (KeyCode.S)) {
			rb.AddForce (-transform.forward * forwardSpeed);
		}

		if (Input.GetKey (KeyCode.A)) {
			rb.AddForce (-transform.right * forwardSpeed);
		}

		if (Input.GetKey (KeyCode.D)) {
			rb.AddForce (transform.right * forwardSpeed);
		}

	}

	void HandleShooting ()
	{
		if (openScreen)
			return;
		if (Input.GetMouseButtonDown (0)) {
			if (currentWeapon == null)
				return;
			
			if (currentWeapon.curMagazineSize > 0) {
				// If it's a burst weapon
				if (currentWeapon.type == Weapon.WeaponTypes.BURST && Time.time > nextFire) {
					nextFire = Time.time + fireRate;
					StartCoroutine (BurstShots ());
				} else
				//if it's a Charge Weapon
					if (currentWeapon.type == Weapon.WeaponTypes.CHARGE) {
					currentCharge = Instantiate (currentWeapon.bulletPrefab, currentWeapon.bulletSpawn.position, currentWeapon.bulletSpawn.rotation);
					currentCharge.transform.localScale = new Vector3 (.5f, .5f, .5f);
					currentCharge.GetComponent<SphereCollider> ().enabled = false;
					currentCharge.transform.SetParent (currentWeapon.bulletSpawn);
					charge = true;
					currentWeapon.PlaySFX (0, false, 0.7f);
				}
				// If it's a normal weapon
					else if (currentWeapon.type != Weapon.WeaponTypes.LASER) {
					currentWeapon.curMagazineSize -= 1;
					currentWeapon.PlaySFX (0, true, 0.7f);
					GameObject go = Instantiate (currentWeapon.bulletPrefab, currentWeapon.bulletSpawn.position, currentWeapon.bulletSpawn.rotation);
					go.GetComponent<Rigidbody> ().AddForce (currentWeapon.bulletSpawn.forward * 2500 * Time.deltaTime, ForceMode.Impulse);
					rb.AddForce (-currentWeapon.bulletSpawn.forward * Time.deltaTime * 200, ForceMode.Impulse);
				}
				// if it's a Laser weapon
				else {
					shootLaser = true;
					currentWeapon.PlaySFX (0, false, 0.7f);
				}

				GameManager.instance.UpdateUI ();
			}
		}
			
		if (Input.GetMouseButton (0)) {
			if (currentWeapon == null)
				return;
			if (currentWeapon.curMagazineSize > 0) {

				if (currentWeapon.type == Weapon.WeaponTypes.LASER) {
					shootLaser = true;
					currentWeapon.curMagazineSize -= 1;
					GameManager.instance.UpdateUI ();
				}
			} 
		}

		if (Input.GetMouseButtonUp (0)) {
			
			if (currentWeapon == null)
				return;
			// Stop Laser
			if (currentWeapon.type == Weapon.WeaponTypes.LASER) {
				if (weaponSpot.childCount != 0) {
					DropTarget ();
					currentWeapon.line.positionCount = 0;
					shootLaser = false;
					currentWeapon.StopSFX ();
				}
			}
			if (currentWeapon.type == Weapon.WeaponTypes.CHARGE) {
				if (currentCharge == null)
					return;
				if (currentCharge.transform.localScale.x < 10f) {
					DestroyCharge ();
				} else {
					ReleaseCharge ();
				}
			}
			shootLaser = false;
		}

		// Pick Up / Drop Items
		if (Input.GetKeyDown (KeyCode.E)) {
			//Check area for Weapons
			AreaCheckForWeapons ();
			// If you have a weapon
			if (currentWeapon != null) {
				// Drop it
				DropWeapon ();
			}
			// If you don't have a weapon
			else {
				if (weaponInRange == null)
					return;
				// Pick up the weapon in range
				currentWeapon = weaponInRange;
				CheckWeaponTypeSetLayer ();
				weaponInRange.transform.position = weaponSpot.position;
				weaponInRange.transform.rotation = weaponSpot.rotation;
				weaponInRange.transform.SetParent (weaponSpot);
				currentWeapon.GetComponent<Rigidbody> ().isKinematic = true;
				currentWeapon.GetComponent<Rigidbody> ().detectCollisions = false;
				GameManager.instance.UpdateUI ();
			}
		} 

		//Reload Weapon
		if (Input.GetKeyDown (KeyCode.R)) {
			if (currentWeapon == null)
				return;
			currentWeapon.StartReload ();

		} 
	}

	void DropWeapon ()
	{
		if (currentWeapon == null)
			return;
		DestroyCharge ();
		currentWeapon.transform.SetParent (null);
		currentWeapon.GetComponent<Rigidbody> ().isKinematic = false;
		currentWeapon.GetComponent<Rigidbody> ().AddForce (currentWeapon.transform.forward * 5f, ForceMode.Impulse);
		currentWeapon.GetComponent<Rigidbody> ().detectCollisions = true;
		currentWeapon = null;
		GameManager.instance.UpdateUI ();
	}

	IEnumerator BurstShots ()
	{
		currentWeapon.PlaySFX (0, true, 0.6f);
		for (int x = 0; x < 3; x++) {
			currentWeapon.curMagazineSize -= 3;
			GameObject go = Instantiate (currentWeapon.bulletPrefab, currentWeapon.bulletSpawn.position, currentWeapon.bulletSpawn.rotation);
			go.GetComponent<Rigidbody> ().AddForce (currentWeapon.bulletSpawn.forward * 400 * Time.deltaTime, ForceMode.Impulse);
			GameObject go1 = Instantiate (currentWeapon.bulletPrefab, currentWeapon.burstSpawns [0].position, currentWeapon.burstSpawns [0].rotation);
			go1.GetComponent<Rigidbody> ().AddForce (currentWeapon.burstSpawns [0].forward * 400 * Time.deltaTime, ForceMode.Impulse);
			GameObject go2 = Instantiate (currentWeapon.bulletPrefab, currentWeapon.burstSpawns [1].position, currentWeapon.burstSpawns [1].rotation);
			go2.GetComponent<Rigidbody> ().AddForce (currentWeapon.burstSpawns [1].forward * 400 * Time.deltaTime, ForceMode.Impulse);
			GameObject go3 = Instantiate (currentWeapon.bulletPrefab, currentWeapon.bulletSpawn.position, currentWeapon.bulletSpawn.rotation);
			go3.GetComponent<Rigidbody> ().AddForce (currentWeapon.bulletSpawn.forward * 400 * Time.deltaTime, ForceMode.Impulse);
			GameObject go4 = Instantiate (currentWeapon.bulletPrefab, currentWeapon.burstSpawns [2].position, currentWeapon.burstSpawns [0].rotation);
			go4.GetComponent<Rigidbody> ().AddForce (currentWeapon.burstSpawns [2].forward * 400 * Time.deltaTime, ForceMode.Impulse);
			GameObject go5 = Instantiate (currentWeapon.bulletPrefab, currentWeapon.burstSpawns [3].position, currentWeapon.burstSpawns [0].rotation);
			go5.GetComponent<Rigidbody> ().AddForce (currentWeapon.burstSpawns [3].forward * 400 * Time.deltaTime, ForceMode.Impulse);
			rb.AddForce (-currentWeapon.bulletSpawn.forward * Time.deltaTime * 500, ForceMode.Impulse);
			yield return new WaitForSeconds (0.1f);
		}
		if (currentWeapon != null)
			currentWeapon.StartReload ();
	}

	void HandleAim ()
	{
		if (!inputEnabled)
			return;
		// Mouse position to World point
		Vector3 v3T = Input.mousePosition;
		v3T.z = Mathf.Abs (Camera.main.transform.position.y - transform.position.y);
		v3T = Camera.main.ScreenToWorldPoint (v3T);
		v3T -= transform.position;
		v3T = v3T * 10000.0f + transform.position;
		transform.LookAt (v3T);

	}

	void HandleLaser ()
	{
		if (currentWeapon == null)
			return;
		if (Input.GetMouseButton (1)) {
			if (currentObject == null)
				return;
			PullTarget ();
		}
		if (currentWeapon.type == Weapon.WeaponTypes.LASER) {
			if (shootLaser) {
				currentWeapon.line.positionCount = 1;
				RaycastHit hit;
				currentWeapon.line.SetPosition (0, currentWeapon.bulletSpawn.position);
				if (Physics.Raycast (currentWeapon.bulletSpawn.position, currentWeapon.bulletSpawn.forward, out hit)) {
					if (currentObject == null) {
						if (hit.collider) {
							currentWeapon.line.positionCount = 2;
							currentWeapon.line.SetPosition (1, hit.point);
						}
						if (hit.collider.tag == "BallTarget") {
							objectSpot.position = hit.point;
							PickUpTarget (hit.collider.gameObject);
						}
					} else {
						currentWeapon.line.positionCount = 2;
						currentWeapon.line.SetPosition (0, currentWeapon.bulletSpawn.position);
						currentWeapon.line.SetPosition (1, objectSpot.position);
					}
				}
			}
		}
	}

	void HandleCharge ()
	{
		if (currentCharge == null)
			return;
		scale += scaleSpeed * Time.deltaTime;
		if (scale > maxScale) {
			scale = maxScale;
		}
		if (chargeDamage < 56)
			chargeDamage += 6 * Time.deltaTime;
		currentCharge.transform.localScale = new Vector3 (scale, scale / 2, scale / 2);
	}

	void ReleaseCharge ()
	{
		charge = false;
		currentCharge.transform.SetParent (null);
		currentCharge.GetComponent<Rigidbody> ().isKinematic = false;
		currentCharge.GetComponent<SphereCollider> ().enabled = true;
		currentCharge.GetComponent<Rigidbody> ().AddForce (currentWeapon.bulletSpawn.forward * 50, ForceMode.Impulse);
		rb.AddForce (-currentWeapon.bulletSpawn.forward * Time.deltaTime * 200, ForceMode.Impulse);
		scale = 1f;
		currentWeapon.StopSFX ();
		currentWeapon.PlaySFX (1, true, 0.7f);
		currentWeapon.curMagazineSize -= 1;
		currentCharge.GetComponent<Bullet> ().Damage += chargeDamage;
		currentCharge = null;
		chargeDamage = 0;
	}

	void DestroyCharge ()
	{
		Destroy (currentCharge);
		currentCharge = null;
		chargeDamage = 0;
		charge = false;
		scale = 1f;
	}

	void HandleAudio ()
	{
		if (Input.GetKeyDown (KeyCode.W)) {
			if (playerAudio.isPlaying)
				return;
			playerAudio.Play ();
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			if (playerAudio.isPlaying)
				return;
			playerAudio.Play ();
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			if (playerAudio.isPlaying)
				return;
			playerAudio.Play ();
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			if (playerAudio.isPlaying)
				return;
			playerAudio.Play ();
		}

		if (Input.GetKeyUp (KeyCode.W)) {
			playerAudio.Stop ();
		}

		if (Input.GetKeyUp (KeyCode.S)) {
			playerAudio.Stop ();
		}

		if (Input.GetKeyUp (KeyCode.A)) {
			playerAudio.Stop ();
		}

		if (Input.GetKeyUp (KeyCode.D)) {
			playerAudio.Stop ();
		}

	}

	void PickUpTarget (GameObject target)
	{
		// Pick up the current Laser target
		currentObject = target;

		currentObject.GetComponent<Target> ().curHealth -= 1;

		target.transform.SetParent (objectSpot);

		target.GetComponent<Rigidbody> ().ResetCenterOfMass ();

		GameManager.instance.MassSetTarget (currentObject);

	}

	void DropTarget ()
	{
		// Drop the current Laser target
		if (currentObject == null)
			return;
		
		GameManager.instance.MassSetTarget (currentObject);

		currentObject.transform.parent = null;

		currentObject = null;

	}

	void PullTarget ()
	{
		// Pull the current Laser target towards the player
		if (currentObject == null)
			return;
		
		objectSpot.position = Vector3.Lerp (objectSpot.position, currentWeapon.bulletSpawn.position, 0.01f);

	}

	void CheckWeaponTypeSetLayer ()
	{
		// Toggle middle layer to not block Raycast(Laser) || Bullets
		if (currentWeapon == null)
			return;
		
		if (currentWeapon.type == Weapon.WeaponTypes.LASER) {
			GameManager.instance.middleBarrier.layer = 2;
		} else {
			GameManager.instance.middleBarrier.layer = 10;
		}

	}

	void AreaCheckForWeapons ()
	{
		Collider[] objectsInRange = Physics.OverlapSphere (transform.position, 1f);
		foreach (Collider col in objectsInRange) {
			Weapon weapon = col.GetComponent<Weapon> ();
			if (weapon != null) {
				weaponInRange = weapon;
			}
		}
	}

	void OnTriggerEnter (Collider col)
	{
		
		if (col.tag == "PickUp") {
			if (col.gameObject.GetComponentInParent<Weapon> () == null)
				return;
			weaponInRange = col.gameObject.GetComponentInParent<Weapon> ();
		}
		if (col.tag == "OpenScreen") {
			openScreen = true;
		}

	}

	void OnTriggerExit (Collider col)
	{
		if (col.tag == "OpenScreen") {
			openScreen = false;
		}
		if (col.tag == "PickUp") {
			if (col.GetComponent<Weapon> () == currentWeapon)
				weaponInRange = null;
		}
	}
}
