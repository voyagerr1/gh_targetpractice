﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	public static GameManager instance;

	public int[] passcode = new int[]{ 3, 6, 9, 9, 4 };

	public string passcodeString;

	public Player player;

	public Transform[] wallSpawnPoints;

	public List<GameObject> targetSpawnPoints;

	public List<GameObject> ballSpawnPoints;

	public List<GameObject> AISpawnPoints;

	public List<GameObject> targetPrefabs;

	public List<GameObject> ballPrefabs;

	public List<GameObject> activeTargets = new List<GameObject> ();

	public List<GameObject> activeAI = new List<GameObject> ();

	public List<WallSpawner> spawners;

	public List<Weapon> weapons;

	public Animator openScreenAnimator;

	public int enemiesKilled, gBallActive, gBallCount, pBallCount;

	public Text ammoText, weaponText, reloadText, countText, descriptionText, endText;

	public Slider reloadSlider, playerHealthSlider;

	public InputField codeInput;

	public GameObject laserBlocker, middleBarrier, endPanel, AIprefab, playerPrefab;

	void Awake ()
	{
		instance = this;
		player = FindObjectOfType<Player> ();

	}

	void Start ()
	{
		ResetSpawners ();
		passcodeString = ArrayToString (passcode, passcodeString);
	}

	void Update ()
	{
		if (player.currentWeapon != null && player.currentWeapon.curMagazineSize <= 0) {
			if (player.currentWeapon.type != Weapon.WeaponTypes.BURST) {
				reloadText.gameObject.SetActive (true);
			}
			Color temp = reloadText.color; 
			temp.a = (Mathf.Sin (Time.time * 8.0f) + 1.0f) / 2.0f;
			reloadText.color = temp;
		}
		if (player.reload) {
			reloadText.gameObject.SetActive (false);
			reloadSlider.value += Time.deltaTime;
			if (reloadSlider.value >= 1.2f) {
				if (player.currentWeapon != null) {
					player.currentWeapon.Reload ();
				}
				reloadSlider.gameObject.SetActive (false);
				UpdateUI ();
			}
		}

	}

	public void AIAggroPlayer ()
	{
		// For every Target, make it chase the current sphere
		GameObject[] targets = activeAI.ToArray ();
		for (int x = 0; x < targets.Length; x++) {
			if (targets [x] != null) {
				if (!targets [x].GetComponent<Target> ().aggro) {
					targets [x].GetComponent<Target> ().agent.destination = player.transform.position;
					targets [x].GetComponent<Target> ().StopAllCoroutines ();
					targets [x].GetComponent<Target> ().StartCoroutine ("ShootTarget");
				}
			}
		}
	}

	public void MassSetTarget (GameObject target)
	{
		// For every Target, make it chase the current sphere
		GameObject[] targets = activeTargets.ToArray ();
		for (int x = 0; x < targets.Length; x++) {
			if (targets [x] != null) {
				targets [x].GetComponent<Target> ().currentTarget = GameManager.instance.player.currentObject;
				targets [x].GetComponent<Target> ().homing = true;
				targets [x].GetComponent<Target> ().homingactivated = true;
			}
		}
	}

	public void DeMassSetTarget (GameObject target)
	{
		// For every Target, set it back to normal
		GameObject[] targets = activeTargets.ToArray ();
		for (int x = 0; x < targets.Length; x++) {
			if (targets [x] != null) {
				targets [x].GetComponent<Target> ().currentTarget = null;
				targets [x].GetComponent<Target> ().homing = false;
				targets [x].GetComponent<Target> ().homingactivated = false;
			}
		}
	}

	public void SpawnTargets ()
	{
		// For every open Platform, if it's empty, spawn a target
		for (int x = 0; x < targetSpawnPoints.ToArray ().Length; x++) {
			if (targetSpawnPoints [x].GetComponent<Platform> ().currentTarget == null) {
				GameObject go = Instantiate (targetPrefabs [Random.Range (0, targetPrefabs.Count)], new Vector3 (targetSpawnPoints [x].transform.position.x, 1.7f, targetSpawnPoints [x].transform.position.z), Quaternion.identity);
				targetSpawnPoints [x].GetComponent<Platform> ().currentTarget = go.GetComponent<Target> ();
				go.GetComponent<Target> ().owner = targetSpawnPoints [x].GetComponent<Platform> ();
			}
		}

		if (activeAI.Count < 5) {
			for (int x = 0; x < AISpawnPoints.ToArray ().Length; x++) {
				GameObject go = Instantiate (AIprefab, new Vector3 (AISpawnPoints [x].transform.position.x, 1.7f, AISpawnPoints [x].transform.position.z), Quaternion.identity);
				activeAI.Add (go);
			}
		}
	}

	public void SpawnBalls ()
	{
		// For every ball spawn point, if it's empty, check ball type and spawn accordingly
		for (int x = 0; x < ballSpawnPoints.ToArray ().Length; x++) {
			if (ballSpawnPoints [x].GetComponent<Platform> ().currentTarget == null) {

				if (gBallActive == 1) {
					GameObject go = Instantiate (ballPrefabs [0], new Vector3 (ballSpawnPoints [x].transform.position.x, 1.7f, ballSpawnPoints [x].transform.position.z), Quaternion.identity);
					ballSpawnPoints [x].GetComponent<Platform> ().currentTarget = go.GetComponent<Target> ();
					go.GetComponent<Target> ().owner = ballSpawnPoints [x].GetComponent<Platform> ();

				} else {
					GameObject go = Instantiate (ballPrefabs [1], new Vector3 (ballSpawnPoints [x].transform.position.x, 1.7f, ballSpawnPoints [x].transform.position.z), Quaternion.identity);
					ballSpawnPoints [x].GetComponent<Platform> ().currentTarget = go.GetComponent<Target> ();
					go.GetComponent<Target> ().owner = ballSpawnPoints [x].GetComponent<Platform> ();
					gBallActive = 1;
				}
			}
		}
	}

	public void UpdateUI ()
	{
		// Update Player UI elements
		playerHealthSlider.value = player.curHealth;
		if (player.currentWeapon != null) {
			reloadSlider.value = 0;
			descriptionText.text = player.currentWeapon.weaponDescription;
			ammoText.text = player.currentWeapon.curMagazineSize.ToString () + "/" + player.currentWeapon.magazineSize.ToString () + " - Ammo";
			weaponText.text = player.currentWeapon.weaponName;
		} else {
			reloadText.gameObject.SetActive (false);
			descriptionText.text = " ";
			ammoText.text = "0/0 - Ammo";
			weaponText.text = "No Weapon";
		}
	}

	public void ActivateReloadSlider ()
	{
		player.reload = true;
	}

	public void RespawnPlayer ()
	{
		GameObject go = Instantiate (playerPrefab, new Vector3 (0, 1.02f, -7.7f), Quaternion.identity);
		player = go.GetComponent<Player> ();
		playerHealthSlider.value = player.Health;
		UpdateUI ();
	}

	public void ResetSpawners ()
	{
		// Turn off and on Moving Wall spawners
		for (int x = 0; x < wallSpawnPoints.Length; x++) {
			wallSpawnPoints [x].GetComponent<WallSpawner> ().CancelInvoke ();
			wallSpawnPoints [x].GetComponent<WallSpawner> ().InvokeRepeating ("SpawnAndMoveWall", 3f, 5f);
		}
	}

	public void ResetGameVariables ()
	{
		// Restart the game
		DestroyAllTargets ();
		ResetSpawners ();
		ResetWeapons ();
		player.currentWeapon = null;
		player.currentObject = null;
		gBallCount = 0;
		pBallCount = 0;
		gBallActive = 1;
		enemiesKilled = 0;
		laserBlocker.SetActive (true);
		player.inputEnabled = true;
		codeInput.text = "";
		Invoke ("SpawnTargets", 1.5f);
		Invoke ("SpawnBalls", 1.5f);
		endPanel.SetActive (false);
		UpdateUI ();
	}

	void ResetWeapons ()
	{
		weapons [0].line.positionCount = 0;
		for (int x = 0; x < weapons.ToArray ().Length; x++) {
			weapons [x].transform.SetParent (null);
			weapons [x].GetComponent<Rigidbody> ().isKinematic = false;
			weapons [x].GetComponent<Rigidbody> ().detectCollisions = true;
			weapons [x].transform.position = weapons [x].weaponSpawn.position;
			weapons [x].Reload ();
			weapons [x].StopSFX ();
			player.shootLaser = false;
		}

	}

	public void CheckKillCount ()
	{
		// Check player kill count and spawn more enemies as they progress
		if (enemiesKilled == 8) {
			codeInput.text = passcode [0].ToString ();
			Invoke ("SpawnTargets", 1.5f);
		}

		if (enemiesKilled == 16) {
			codeInput.text += passcode [1].ToString ();
			Invoke ("SpawnTargets", 1.5f);
		}

		if (enemiesKilled == 22) {
			codeInput.text += passcode [2].ToString ();
			Invoke ("SpawnTargets", 1.5f);
		}

		if (enemiesKilled == 30) {
			codeInput.text += passcode [3].ToString ();
			Invoke ("SpawnTargets", 1.5f);
		}

		if (enemiesKilled == 38) {
			codeInput.text += passcode [4].ToString ();
			Invoke ("SpawnTargets", 1.5f);
		}

		if (enemiesKilled == 46) {
			codeInput.text += passcode [5].ToString ();
			laserBlocker.SetActive (false);
			Invoke ("SpawnTargets", 1.5f);
		}
	}

	public void CheckInputCode ()
	{
		// Checks input field value vs. code value
		string input = codeInput.text;
		if (input != passcodeString) {
			return;
		} else {
			laserBlocker.SetActive (false);
		}
	}

	public void CheckBallCount ()
	{
		if (gBallCount >= 1 && pBallCount >= 1) {
			player.currentWeapon.line.positionCount = 0;
			player.shootLaser = false;
			player.currentWeapon.StopSFX ();
			player.inputEnabled = false;
			endPanel.SetActive (true);
		}
	}

	string ArrayToString (int[] array, string text)
	{
		// For every number in the array, add it to the same string
		for (int x = 0; x < array.Length; x++) {
			text += array [x].ToString ();

		}
		return text;
	}

	void SpawnWall (WallSpawner wall)
	{
		wall.InvokeRepeating ("SpawnAndMoveWall", 1f, 4.5f);
	}

	void DestroyAllTargets ()
	{
		for (int x = 0; x < activeTargets.ToArray ().Length; x++) {
			Destroy (activeTargets [x]);
		}
		for (int x = 0; x < activeAI.ToArray ().Length; x++) {
			Destroy (activeAI [x]);
		}
		activeTargets.Clear ();
		activeAI.Clear ();
	}

}
