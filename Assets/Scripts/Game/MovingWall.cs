﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{

	public WallSpawner.Directions direction;
	public WallSpawner spawner;
	public float wallSpeed;
	private Rigidbody rb;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
	}

	void Update ()
	{
		Move ();
	}

	void Move ()
	{
		if (direction == WallSpawner.Directions.LEFT) {
			rb.AddForce (Vector3.left * Time.deltaTime * wallSpeed, ForceMode.Acceleration);
		} else {
			rb.AddForce (Vector3.right * Time.deltaTime * wallSpeed, ForceMode.Acceleration);
		}
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.tag == "Death") {
			Destroy (gameObject);
		}
	}
}
