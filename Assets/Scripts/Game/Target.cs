﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


public class Target : MonoBehaviour
{

	public enum TargetTypes
	{
		NORMAL,
		BALL,
		AI
	}

	public enum BallTypes
	{
		PURPLE,
		GREEN
	}

	public TargetTypes targetType;
	public BallTypes ballType;
	public int Health, curHealth;
	public GameObject deathPrefab;
	public bool homing, aggro;
	public DropZone.DropZones pickUptype;
	private Slider healthbar;
	public bool homingactivated;
	public Platform owner;
	public GameObject currentTarget;
	public AudioSource targetAudio;
	[HideInInspector]
	public NavMeshAgent agent;
	private Weapon weapon;


	void Start ()
	{
		if (targetType == TargetTypes.AI) {
			agent = GetComponent<NavMeshAgent> ();
			weapon = GetComponentInChildren<Weapon> ();
		}
		targetAudio = SFXManager.instance.mainAudio;
		InitTarget ();
		InvokeRepeating ("CheckRotation", 3f, 3f);
	}

	void Update ()
	{
		if (targetType == TargetTypes.AI) {
			if (GameManager.instance.player == null)
				return;
			transform.LookAt (GameManager.instance.player.transform);
		}

		if (!homing || !homingactivated || currentTarget == null)
			return;
		SeekTarget (currentTarget);
	}

	void InitTarget ()
	{
		
		healthbar = GetComponentInChildren<Slider> ();
		healthbar.maxValue = Health;
		healthbar.value = Health;
		curHealth = Health;
		if (targetType == TargetTypes.NORMAL) {
			GameManager.instance.activeTargets.Add (gameObject);
		} else {
			
		}
	}


	void SeekTarget (GameObject target)
	{
		Vector3 relativePos = target.transform.position - transform.position;
		Quaternion rotation = Quaternion.LookRotation (relativePos);
		transform.rotation = Quaternion.Slerp (transform.rotation, rotation, 2f);
		transform.Translate (0, 0, 3.5f * Time.deltaTime, Space.Self);
	}


	IEnumerator ShootTarget ()
	{
		aggro = true;
		yield return new WaitForSeconds (1.5f);
		weapon.curMagazineSize -= 1;
		weapon.PlaySFX (0, true, 0.2f);
		GameObject go = Instantiate (weapon.bulletPrefab, weapon.bulletSpawn.position, weapon.bulletSpawn.rotation);
		go.GetComponent<Rigidbody> ().AddForce (weapon.bulletSpawn.forward * 2500 * Time.deltaTime, ForceMode.Impulse);
		GetComponent<Rigidbody> ().AddForce (-weapon.bulletSpawn.forward * Time.deltaTime * 200, ForceMode.Impulse);

		yield return new WaitForSeconds (1.5f);
		StartCoroutine ("ShootTarget");
	}

	public void UpdateHealthbar ()
	{
		StartCoroutine (SwitchMat ());
		healthbar.value = curHealth;
		if (curHealth != Health && homing) {
			homingactivated = true;
		} else if (targetType == TargetTypes.AI) {
			if (curHealth != Health) {
				GameManager.instance.AIAggroPlayer ();
			}
		}

		if (curHealth <= 0) {
			if (targetType == TargetTypes.BALL) {
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
				targetAudio.Play ();
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}

			if (targetType == TargetTypes.AI) {
				GameManager.instance.activeAI.Remove (gameObject);
				GameManager.instance.enemiesKilled += 1;
				GameManager.instance.CheckKillCount ();
				targetAudio.Play ();
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			} else {
				GameManager.instance.enemiesKilled += 1;
				GameManager.instance.CheckKillCount ();
				owner.currentTarget = null;
				GameManager.instance.activeTargets.Remove (gameObject);
				targetAudio.Play ();
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}
		}
	}

	IEnumerator SwitchMat ()
	{
		Color temp = GetComponent<Renderer> ().material.color;
		GetComponent<Renderer> ().material.color = Color.red;
		yield return new WaitForSeconds (.25f);
		GetComponent<Renderer> ().material.color = temp;

	}

	void CheckRotation ()
	{
		int current = (int)transform.rotation.x;
		if (current >= 40) {
			StartCoroutine (WaitDestroy ());
		}
	}

	IEnumerator WaitDestroy ()
	{
		targetAudio.Play ();
		yield return new WaitForSeconds (1f);
		Instantiate (deathPrefab, transform.position, Quaternion.identity);
		Destroy (gameObject);
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.tag == "DropZone") {
			if (pickUptype == col.GetComponent<DropZone> ().type) {
				if (pickUptype == DropZone.DropZones.GREEN) {
					GameManager.instance.gBallCount += 1;
					GameManager.instance.gBallActive = 0;
				} else {
					GameManager.instance.pBallCount += 1;
				}
				GameManager.instance.CheckBallCount ();
				targetAudio.Play ();
				GameManager.instance.UpdateUI ();
				GameManager.instance.activeTargets.Remove (gameObject);
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}
		}

		if (col.tag == "Barrier") {
			if (targetType == TargetTypes.AI)
				return;
			targetAudio.Play ();
			if (targetType == TargetTypes.BALL) {
				if (ballType == BallTypes.GREEN) {
					GameManager.instance.gBallActive = 0;
				}
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
			} else {
				GameManager.instance.activeTargets.Remove (gameObject);
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}
		
		}

		if (col.tag == "World") {
			if (targetType == TargetTypes.AI)
				return;
			targetAudio.Play ();
			if (targetType == TargetTypes.BALL) {
				if (ballType == BallTypes.GREEN) {
					GameManager.instance.gBallActive = 0;
				}
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
			} else {
				GameManager.instance.enemiesKilled += 1;
				GameManager.instance.CheckKillCount ();
				GameManager.instance.activeTargets.Remove (gameObject);
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}
		}

		if (col.tag == "DoubleCost") {
			if (targetType == TargetTypes.AI)
				return;
			targetAudio.Play ();
			if (targetType == TargetTypes.BALL) {
				if (ballType == BallTypes.GREEN) {
					GameManager.instance.gBallActive = 0;
				}
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
			} else {
				GameManager.instance.enemiesKilled += 1;
				GameManager.instance.CheckKillCount ();
			}
			GameManager.instance.activeTargets.Remove (gameObject);
			Instantiate (deathPrefab, transform.position, Quaternion.identity);
			Destroy (gameObject);

		}
	}

	void OnCollisionEnter (Collision col)
	{

		if (col.gameObject.tag == "World") {
			if (targetType == TargetTypes.AI)
				return;
			targetAudio.Play ();
			if (targetType == TargetTypes.BALL) {
				if (ballType == BallTypes.GREEN) {
					GameManager.instance.gBallActive = 0;
				}
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
			} else {
				GameManager.instance.enemiesKilled += 1;
				GameManager.instance.CheckKillCount ();
				GameManager.instance.activeTargets.Remove (gameObject);
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}
			
		}

		if (col.gameObject.tag == "DoubleCost") {
			if (targetType == TargetTypes.AI)
				return;
			targetAudio.Play ();
			if (targetType == TargetTypes.BALL) {
				if (ballType == BallTypes.GREEN) {
					GameManager.instance.gBallActive = 0;
				}
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
			}
			GameManager.instance.activeTargets.Remove (gameObject);
			Instantiate (deathPrefab, transform.position, Quaternion.identity);
			Destroy (gameObject);
		}

		if (col.gameObject.tag == "BallTarget") {
			if (homing) {
				targetAudio.Play ();
				if (targetType == TargetTypes.BALL) {
					if (ballType == BallTypes.GREEN) {
						GameManager.instance.gBallActive = 0;
					}
					GameManager.instance.Invoke ("SpawnBalls", 1.5f);
				} else {
					GameManager.instance.enemiesKilled += 1;
					GameManager.instance.CheckKillCount ();
				}

				if (targetType == TargetTypes.AI) {
					curHealth -= 20;
					UpdateHealthbar ();
				} else {
					GameManager.instance.activeTargets.Remove (gameObject);
				}
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}
		}

		if (col.gameObject.tag == "Target") {
			if (targetType == TargetTypes.AI)
				return;
			if (targetType == TargetTypes.BALL) {
				if (ballType == BallTypes.GREEN) {
					GameManager.instance.gBallActive = 0;
				}
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
				GameManager.instance.activeTargets.Remove (gameObject);
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}

		}
		if (col.gameObject.tag == "Barrier") {
			if (targetType == TargetTypes.AI)
				return;
			targetAudio.Play ();
			if (targetType == TargetTypes.BALL) {
				if (ballType == BallTypes.GREEN) {
					GameManager.instance.gBallActive = 0;
				}
				GameManager.instance.Invoke ("SpawnBalls", 1.5f);
			} else {
				GameManager.instance.enemiesKilled += 1;
				GameManager.instance.CheckKillCount ();
				GameManager.instance.activeTargets.Remove (gameObject);
				Instantiate (deathPrefab, transform.position, Quaternion.identity);
				Destroy (gameObject);
			}
		}
	}


}
