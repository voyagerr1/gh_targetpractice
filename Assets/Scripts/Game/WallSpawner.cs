﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSpawner : MonoBehaviour
{
	public enum Directions
	{
		LEFT,
		RIGHT
	}

	public Directions direction;
	public GameObject wallPrefab;
	public GameObject[] walls;
	// Use this for initialization
	void Start ()
	{
		
	}



	void SpawnAndMoveWall ()
	{
		GameObject go = Instantiate (wallPrefab, transform.position, Quaternion.identity);
		go.GetComponent<MovingWall> ().direction = direction;
		go.GetComponent<MovingWall> ().spawner = this;
	}
}
