﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

	public enum WeaponTypes
	{
		RIFLE,
		BURST,
		LASER,
		CHARGE
	}

	public string weaponName;
	[TextArea (5, 20)]
	public string weaponDescription;

	public WeaponTypes type;

	public GameObject bulletPrefab;

	public Transform bulletSpawn, weaponSpawn;

	public Transform[] burstSpawns;

	public LineRenderer line;

	public int magazineSize, curMagazineSize;

	public AudioClip[] sounds;

	private AudioSource weaponAudio;

	void Start ()
	{
		weaponAudio = GetComponent<AudioSource> ();
	}

	public void StartReload ()
	{
		PlaySFX (1, true, 0.4f);

		GameManager.instance.reloadSlider.gameObject.SetActive (true);
		GameManager.instance.ActivateReloadSlider ();
	}

	public void Reload ()
	{
		curMagazineSize = magazineSize;
		GameManager.instance.UpdateUI ();
		GameManager.instance.player.reload = false;
	}

	public void PlaySFX (int sound, bool oneshot, float volume)
	{
		if (!oneshot) {
			weaponAudio.clip = sounds [sound];
			weaponAudio.Play ();
		} else {
			weaponAudio.clip = sounds [sound];
			weaponAudio.PlayOneShot (sounds [sound], volume);
		}
	}

	public void StopSFX ()
	{
		weaponAudio.Stop ();
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.tag == "Ground") {
			PlaySFX (2, true, 0.25f);
		}
	}
}
