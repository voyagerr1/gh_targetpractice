﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float Damage;
	public GameObject destroyPrefab;
	public int bulletLife;
	public float knockBack;

	void Start ()
	{
		
	}

	void OnCollisionEnter (Collision col)
	{
		
		if (col.gameObject.tag == "Target") {
			col.gameObject.GetComponent<Target> ().curHealth -= (int)Damage;
			col.gameObject.GetComponent<Target> ().UpdateHealthbar ();
			if (GameManager.instance.player.currentWeapon != null && GameManager.instance.player.currentWeapon.type != Weapon.WeaponTypes.BURST) {
				col.gameObject.GetComponent<Rigidbody> ().AddForce (Vector3.back * knockBack * Time.deltaTime, ForceMode.Impulse);
			}
			DestroyBullet ();
		}

		if (col.gameObject.tag == "Player") {
			col.gameObject.GetComponent<Player> ().curHealth -= (int)Damage;
			col.gameObject.GetComponent<Player> ().UpdateHealthbar ();
			col.gameObject.GetComponent<Rigidbody> ().AddForce (Vector3.back * (knockBack / 2) * Time.deltaTime, ForceMode.Impulse);
			DestroyBullet ();
		}

		if (col.gameObject.tag == "DoubleCost") {

			DestroyBullet ();
		}

		if (col.gameObject.tag == "Ground") {

			DestroyBullet ();
		}

		if (col.gameObject.tag == "Platform") {

			DestroyBullet ();
		}

		if (col.gameObject.tag == "World") {
			
			bulletLife -= 2;
			Damage += (Damage * 5);
			CheckBullet ();
		}

		if (col.gameObject.tag == "Bullet") {
			DestroyBullet ();
		}

		if (col.gameObject.tag == "BallTarget") {
			col.gameObject.GetComponent<Target> ().currentTarget = GameManager.instance.player.gameObject;
			col.gameObject.GetComponent<Target> ().curHealth -= 1;
			col.gameObject.GetComponent<Target> ().UpdateHealthbar ();
			col.gameObject.GetComponent<Rigidbody> ().isKinematic = false;
			DestroyBullet ();
		}

	}

	void DestroyBullet ()
	{
		Instantiate (destroyPrefab, transform.position, Quaternion.identity);
		Destroy (gameObject);
	}

	void CheckBullet ()
	{
		if (bulletLife <= 0) {
			DestroyBullet ();
		}
	
	}
}
